package com.daffodil.assignment.listeners;

import android.widget.ImageView;

public interface OptionMenuCallBackInterface {
    void onFinish(ImageView imageView);
}
