package com.daffodil.assignment.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.daffodil.assignment.R;

public class ViewHolderAdvertisement extends RecyclerView.ViewHolder {
    public ImageView advertisement;

    public ViewHolderAdvertisement(View itemView) {
        super(itemView);
        advertisement = (ImageView) itemView.findViewById(R.id.addImageView);
    }
}
