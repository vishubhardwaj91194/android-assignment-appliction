package com.daffodil.assignment.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.R;
import com.daffodil.assignment.dataholders.VHAdvertisement;
import com.daffodil.assignment.dataholders.VHBaseDetails;
import com.daffodil.assignment.dataholders.VHDetailHeader;
import com.daffodil.assignment.dataholders.VHSimilarItems;
import com.daffodil.assignment.dataholders.VHWebView;
import com.daffodil.assignment.helpers.HelperMethods;
import com.daffodil.assignment.viewholders.ViewHolder;
import com.daffodil.assignment.viewholders.ViewHolderAdvertisement;
import com.daffodil.assignment.viewholders.ViewHolderWebView;

import java.util.ArrayList;

public class SecondAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<VHBaseDetails> detailsArrayList;
    private Context context;
    private int length;
    private LayoutInflater inflater;

    public SecondAdapter(ArrayList<VHBaseDetails> passedArrayList, Context passedContext) {
        this.detailsArrayList = passedArrayList;
        this.context = passedContext;
        // this.length = passedArrayList.size();
        this.length = 0;
        inflater = LayoutInflater.from(passedContext);
    }

    public void setDetailsArrayList(ArrayList<VHBaseDetails> detailsArrayList) {
        this.detailsArrayList = detailsArrayList;
        this.length = detailsArrayList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View returnedView;
        RecyclerView.ViewHolder returnedViewHolder;
        if(viewType == ApplicationConstants.VIEW_TYPE_FIRST){
            returnedView = inflater.inflate(R.layout.first_post, parent, false);
            returnedView.findViewById(R.id.add_to_my_box).setVisibility(View.VISIBLE);
            returnedViewHolder = new ViewHolder(returnedView, false);
        }else if(viewType == ApplicationConstants.VIEW_TYPE_SECOND){
            returnedView = inflater.inflate(R.layout.second_post, parent, false);
            returnedViewHolder = new ViewHolderWebView(returnedView);
        }else if(viewType == ApplicationConstants.VIEW_TYPE_THIRD){
            returnedView = inflater.inflate(R.layout.third_post, parent, false);
            returnedViewHolder = new ViewHolderAdvertisement(returnedView);
        }else{
            returnedView = inflater.inflate(R.layout.next_post, parent, false);
            returnedViewHolder = new ViewHolder(returnedView, true);
        }
        return returnedViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == ApplicationConstants.VIEW_TYPE_FIRST){
            ViewHolder holder1 = (ViewHolder)holder;
            VHDetailHeader detailHeader = (VHDetailHeader) detailsArrayList.get(position);
            /*
                holder1.title.setText(detailHeader.title);
                holder1.tag11.setText(detailHeader.author);
                holder1.tag12.setText(detailHeader.scienceName);
                holder1.date1.setText(detailHeader.dop);
                holder1.sm_1.setText(detailHeader.fbCount);
                holder1.sm_2.setText(detailHeader.twitterCount);

                // set item id in view holde
                String id = String.valueOf(detailHeader._id);
                holder1.itemId.setText(id);

                HelperMethods.picassoLoadImage(holder1.image_view,detailHeader.image,context);
            */
            holder1.setViewForHeader(detailHeader);
        }else if(position == ApplicationConstants.VIEW_TYPE_SECOND){
            ViewHolderWebView holder1 = (ViewHolderWebView)holder;
            VHWebView detailHeader = (VHWebView) detailsArrayList.get(position);
            holder1.setListener(detailHeader.getListener());
            // holder1.webView.loadData(detailHeader.vewHtml, "text/html", "UTF-8");
            //holder1.webView.loadDataWithBaseURL("", detailHeader.vewHtml, "text/html", "UTF-8", "");
            holder1.webView.loadData(Html.fromHtml(detailHeader.vewHtml).toString(), "text/html", "UTF-8");

        }else if(position == ApplicationConstants.VIEW_TYPE_THIRD){
            ViewHolderAdvertisement holder1 = (ViewHolderAdvertisement) holder;
            VHAdvertisement detailHeader = (VHAdvertisement) detailsArrayList.get(position);

            HelperMethods.picassoLoadImage(holder1.advertisement, detailHeader.advertisementUrl, context);

        }else{
            ViewHolder holder1 = (ViewHolder)holder;
            VHSimilarItems detailHeader = (VHSimilarItems) detailsArrayList.get(position);
           /*
                holder1.title.setText(detailHeader.title);
                holder1.tag11.setText(detailHeader.author);
                holder1.tag12.setText(detailHeader.scienceName);
                holder1.date1.setText(detailHeader.dop);
                holder1.sm_1.setText(detailHeader.fbCount);
                holder1.sm_2.setText(detailHeader.twitterCount);

                // set item id in view holde
                String id = String.valueOf(detailHeader._id);
                holder1.itemId.setText(id);

                HelperMethods.picassoLoadImage(holder1.image_view,detailHeader.image,context);
            */
            holder1.setViewForSimilar(detailHeader);
        }
    }

    @Override
    public int getItemCount() {
        return length;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == ApplicationConstants.VIEW_TYPE_FIRST){
            return ApplicationConstants.VIEW_TYPE_FIRST;
        }else if(position == ApplicationConstants.VIEW_TYPE_SECOND){
            return ApplicationConstants.VIEW_TYPE_SECOND;
        }else if(position == ApplicationConstants.VIEW_TYPE_THIRD){
            return ApplicationConstants.VIEW_TYPE_THIRD;
        }else {
            return (ApplicationConstants.VIEW_TYPE_THIRD + 1);
        }
    }
}
