package com.daffodil.assignment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.daffodil.assignment.helpers.ProgressDialogUtil;

public class WebViewFragment extends Fragment {

    private String url;
    private ProgressDialogUtil mDialogUtil;
    private WebView mWebView;

    public static WebViewFragment createInstance(String url) {
        WebViewFragment fragment = new WebViewFragment();
        fragment.url = url;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View inflatedView = inflater.inflate(R.layout.webview_fragment, container, false);
        mDialogUtil = new ProgressDialogUtil(getActivity());
        mWebView = (WebView) inflatedView.findViewById(R.id.wv_web_view_layout);
        return inflatedView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mWebView.getUrl() == null) {

            setContentIntoWebView();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setContentIntoWebView() {

        String urlToLoad = this.url;

        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        if (urlToLoad != null) {

//            if (urlToLoad.startsWith("file://")) {
//
//                if (mIntents != null) {
//
//                    mDialogUtil.showProgress();
//                    mWebView.loadUrl(urlToLoad);
//                    mWebView.setWebViewClient(new CustomWebViewClient());
//                }
//            } else
            if ((urlToLoad.startsWith("http://") || urlToLoad.startsWith("https://"))) {

                mDialogUtil.showProgress();
                mWebView.loadUrl(urlToLoad);
                mWebView.setWebViewClient(new CustomWebViewClientForHttp());
            }
        } else {

            throw new IllegalArgumentException("url can not be null");
        }
    }

//    public class CustomWebViewClient extends WebViewClient {
//
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            if (mIntents != null) {
//                for (int i = 0; i < mIntents.size(); i++) {
//
//                    if (mIntents.get(i).canHandle(url)) {
//
//                        mDialogUtil.hideProgress();
//                        startActivity(mIntents.get(i).configure(url));
//                        return true;
//                    }
//                }
//            } else {
//                throw new IllegalArgumentException("list can not be empty");
//            }
//            return false;
//        }
//
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//            mDialogUtil.hideProgress();
//        }
//    }

    public class CustomWebViewClientForHttp extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mDialogUtil.hideProgress();
        }
    }

    /**
     * @return false if activity needs to manage back press usually by calling super
     * else returns true
     */
    public boolean onActivityBackPressed() {

        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }
}