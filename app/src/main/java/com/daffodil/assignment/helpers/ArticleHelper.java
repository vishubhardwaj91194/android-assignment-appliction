package com.daffodil.assignment.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.listeners.DetilsDownloadListener;

import java.io.IOException;
import java.util.ArrayList;

public class ArticleHelper {

//    public static ArrayList<Article> getDataFromSdCard() {
//        ArrayList<Article> articleList=null;
//        try {
//            articleList=JsonUtil.jsonDataToArrayList(ArticleHelper.runSd());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return articleList;
//    }

//    public static ArrayList<ArticleRealm> getDataFromNet(){
//        ArrayList<ArticleRealm> articleList=null;
//        int skip = 0, limit =10;
//        try {
//            articleList = JsonUtil.jsonDataToArrayList2(ArticleHelper.run(skip,limit));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return articleList;
//    }

//    private static String run(int skip, int limit) throws IOException {
////        String url = "http://knowlarity.izofy.com/izofyapi/api/article/getarticlelist?categoryid=25&practice=2&problem=0&skip="+skip+"&limit="+limit+"&apiKey=fac56a890f51471b9be8600e550c262adf64efac93cc4d22a4c84b84801207f7";
////        OkHttpClient client = new OkHttpClient();
////        Request request = new Request.Builder()
////                .url(url)
////                .build();
////
////        Response response = client.newCall(request).execute();
////        if(response.isSuccessful()){
////            return response.body().string();
////        }else{
////            throw new IOException("Unexpected code " + response);
////        }
//        return null;
//    }


    public static void runValle(int skip, int limit, Context context, com.android.volley.Response.Listener listener) throws  IOException{

        String url = "http://knowlarity.izofy.com/izofyapi/api/article/getarticlelist?categoryid=25&practice=2&problem=0&skip="+skip+"&limit="+limit+"&apiKey=fac56a890f51471b9be8600e550c262adf64efac93cc4d22a4c84b84801207f7";
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(url, listener, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EEEEEEEE",error.toString());
            }
        }
        );
        queue.add(stringRequest);
    }

//    private static String runSd() throws IOException{
//        String next=Environment.getExternalStorageDirectory().getAbsolutePath();
//        File myFile = //new File("/sdcard/mysdfile.txt");
//                new File(next, "jsonFile.json");
//        if(myFile.exists()){
//            System.out.println(next);
//            FileInputStream fIn = new FileInputStream(myFile);
//            BufferedReader myReader = new BufferedReader(
//                    new InputStreamReader(fIn));
//            String aDataRow;
//            String aBuffer = "";
//            while ((aDataRow = myReader.readLine()) != null) {
//                aBuffer += aDataRow + "\n";
//            }
//            myReader.close();
//            return  aBuffer;
//        }else{
//            throw new IOException("Unexpected code : File does not exist");
//        }
//    }
//
//
//    public static void addArticleHelper(ArticleRealm passedArticle, Context context){
//        ArticleDataManager datasource = new ArticleDataManager(context);
//        datasource.open();
//        AddArticleCompleted listener = new AddArticleCompleted() {
//            @Override
//            public void onFinish(String id) {
//                // do something when finished to add article;
//                Log.d("Article inserted : ","Id = "+id);
//            }
//        };
//        datasource.addArticle(passedArticle, listener);
//    }

//    public static void getAllArticles(GetAllArticleCompleted listener, Context context){
//        ArticleDataManager datasource = new ArticleDataManager(context);
//        datasource.open();
//        datasource.getAllArticle(listener);
//    }

//    public static void getArticleForId(GetArticleForIdCompleted listener, Context context, long id){
//        ArticleDataManager datasource = new ArticleDataManager(context);
//        datasource.open();
//        datasource.getArticleForId(listener, id);
//    }

    public interface AddArticleCompleted{
        void onFinish(String id);
    }

    public interface GetAllArticleCompleted{
        void onFinish(ArrayList<ArticleRealm> articles);
    }

    public interface GetArticleForIdCompleted {
        void onFinish(ArticleRealm a);
    }

//    public static void exportDB(Context context){
//        File sd = Environment.getExternalStorageDirectory();
//        File data = Environment.getDataDirectory();
//        FileChannel source=null;
//        FileChannel destination=null;
//        String currentDBPath = "/data/"+ "com.daffodil.assignment" +"/databases/"+DatabaseManager.DATABASE_NAME;
//        String backupDBPath = DatabaseManager.DATABASE_NAME;
//        File currentDB = new File(data, currentDBPath);
//        File backupDB = new File(sd, backupDBPath);
//        try {
//            source = new FileInputStream(currentDB).getChannel();
//            destination = new FileOutputStream(backupDB).getChannel();
//            destination.transferFrom(source, 0, source.size());
//            source.close();
//            destination.close();
//            Toast.makeText(context, "DB Exported!", Toast.LENGTH_LONG).show();
//        } catch(IOException e) {
//            e.printStackTrace();
//        }
//    }

    public interface AddAllArticleCompleted {
        void onFinish(ArrayList<ArticleRealm> passedArticles);
    }

//    public static void addAllArticleDataHelper(ArrayList<ArticleRealm> articles, ArticleHelper.AddAllArticleCompleted articleCompletedListener,Context context){
//        ArticleDataManager datasource = new ArticleDataManager(context);
//        datasource.open();
//        datasource.addAllArticles(articles, articleCompletedListener);
//    }

    public static void fetchDetilData(int item_id, final DetilsDownloadListener listener, Context context) {
        DetailDownload localListener = new DetailDownload() {
            @Override
            public void onDetailDownloaded(String detailHeader) {
                listener.onDetailArticleReceived(detailHeader);
            }

            @Override
            public void onSimilarArticlesDownloaded(String detailSimilar) {
                listener.onSimilarArticlesReceived(detailSimilar);
            }

            @Override
            public void onError() {
                listener.onFailure();
            }
        };
        fetchDetailsScreen(item_id, localListener, context);
        fetchSimilarArticles(item_id, localListener, context);
    }

    private interface DetailDownload{
        void onDetailDownloaded(String detailHeader);
        void onSimilarArticlesDownloaded(String detailSimilar);
        void onError();
    }

    public static void fetchDetailsScreen(int item_id, final DetailDownload listener, Context context){
        String url = UrlHelper.getDetailArticleUrl(item_id);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(
            url,
            new com.android.volley.Response.Listener<String>(){
                @Override
                public void onResponse(String response) {
                    listener.onDetailDownloaded(response);
                }
            },
            new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("EEEEEEEE",error.toString());
                    listener.onError();
                }
            }
        );
        queue.add(stringRequest);
    }

    public static void fetchSimilarArticles(int item_id, final DetailDownload listener, Context context){
        String url = UrlHelper.getSimilarArticleUrl(item_id);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(
                url,
                new com.android.volley.Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        listener.onSimilarArticlesDownloaded(response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("EEEEEEEE",error.toString());
                        listener.onError();
                    }
                }
        );
        queue.add(stringRequest);
    }

    public static void fetchDataByUrl(String url,final UrlDownload listener, Context context){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(
                url,
                new com.android.volley.Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        listener.onSuccess(response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("EEEEEEEE",error.toString());
                        listener.onFailure();
                    }
                }
        );
        queue.add(stringRequest);
    }

    public static interface UrlDownload{
        void onSuccess(String data);
        void onFailure();
    }
}
