package com.daffodil.assignment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.daffodil.assignment.dataholders.ApplicationConstants;

public class WebViewActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String url;
    private WebViewFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_view);
        initToolbar();
        Intent passedIntent = getIntent();
        this.url = passedIntent.getStringExtra(ApplicationConstants.PASSED_URL);

        fragment = WebViewFragment.createInstance(url);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.wv_web_view_layout, fragment, "web view")
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            // toolbar.setLogo(R.drawable.toolbar_logo);
            toolbar.setTitle("");
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    finish();
//                }
//            });
        }else{
            toolbar.setTitle("asas");
        }
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (fragment == null
                || !fragment.onActivityBackPressed()) {
            super.onBackPressed();
        }
    }

    //    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_web_view);
//        initToolbar();
//        Intent passedIntent = getIntent();
//        url = passedIntent.getStringExtra(ApplicationConstants.PASSED_URL);
//        webView = (WebView) findViewById(R.id.web_view_layout);
//
//        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//
////        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0"); // linux desktop mozilla user agent
////        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        webView.clearHistory();
//        webView.loadUrl(url);
//
//        loadUrl(url);
//    }
//
//
//
//    public void loadUrl(String url){
//        /*final WebView */
//
//        if (webView != null) {
//
//
//            // display progress bar
////            final ProgressDialog progressDialog=new ProgressDialog(this);
////            progressDialog.setMessage(getString(R.string.downloading_notice2));
////            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
////            progressDialog.setIndeterminate(false);
////            progressDialog.show();
////            urlList.add(url);
//
//
//            // Trying test code
////            final ProgressDialog progressDialog=new ProgressDialog(this);
////            progressDialog.setMessage(getString(R.string.downloading_notice2));
////            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
////            progressDialog.setIndeterminate(false);
////            progressDialog.show();
////            ArticleHelper.UrlDownload listener = new ArticleHelper.UrlDownload(){
////
////                @Override
////                public void onSuccess(String data) {
////                    progressDialog.dismiss();
////                    //webView.loadData(Html.fromHtml(data).toString(), "text/html", "UTF-8");
////                    webView.loadData(data, "text/html", "UTF-8");
////                }
////
////                @Override
////                public void onFailure() {
////                    progressDialog.dismiss();
////                    finish();
////                }
////            };
////            ArticleHelper.fetchDataByUrl(url, listener, this);
////            final WebViewClient webviewclient = new WebViewClient(){
////                @Override
////                public void onPageFinished(WebView view, String url) {
////                    super.onPageFinished(view, url);
//////                    if(progressDialog.isShowing()){
////                    progressDialog.dismiss();
//////                    }
////                }
////                @Override
////                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
////                    super.onReceivedError(view, request, error);
////                    Toast.makeText(getApplicationContext(), "asdsahdgajhdsgjhasd"+error.toString(), Toast.LENGTH_SHORT).show();
////                    progressDialog.dismiss();
////                }
////
////                @Override
////                public boolean shouldOverrideUrlLoading(WebView view, String url) {
////                    view.loadUrl(url);
////                    return false;
////                }
////
////                @Override
////                public void onPageStarted(WebView view, String url, Bitmap favicon) {
////                    super.onPageStarted(view, url, favicon);
////                }
////
////                @Override
////                public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
////                    super.doUpdateVisitedHistory(view, url, isReload);
////                }
////
////            };
////            webView.setWebViewClient(webviewclient);
//////            webView.setWebChromeClient(new WebChromeClient() {});
////
////            webView.loadUrl(url);
//                webView.setWebViewClient(new CustomWebViewClient());
//        }
//    }
//
//    private class CustomWebViewClient extends WebViewClient{
//        @Override
//        public void onPageFinished(WebView view, String url) {
//            super.onPageFinished(view, url);
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        if (webView.canGoBack()) {
//            webView.goBack();
////            return true;
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    /*public boolean onActivityBackPressed() {
//
//        if (webView.canGoBack()) {
//            webView.goBack();
//            return true;
//        } else {
//            return false;
//        }
//    }*/
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        webView.onPause();
//    }
//
//
//
////    @Override
////    public void onBackPressed() {
////        super.onBackPressed();
////
////    }
////    @Override
////    public void onBackPressed()
////    {
////        if(webView.canGoBack()){
//////            ArrayList<String> arrayList = new ArrayList<>();
//////            for(int i=0;i<webView.copyBackForwardList().getSize();i++){
//////                arrayList.add(webView.copyBackForwardList().getItemAtIndex(i).getUrl());
//////            }
////            webView.goBack();
////        }else{
////            super.onBackPressed();
////        }
////    }
//
//    /*@Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if(event.getAction() == KeyEvent.ACTION_DOWN){
//            switch(keyCode)
//            {
//                case KeyEvent.KEYCODE_BACK:
//                    if(webView.canGoBack() == true){
//                        ArrayList<String> arrayList = new ArrayList<>();
//                        for(int i=0;i<webView.copyBackForwardList().getSize();i++){
//                            arrayList.add(webView.copyBackForwardList().getItemAtIndex(i).getUrl());
//                        }
//                        webView.goBack();
//                    }else{
//                        finish();
//                    }
//                    return true;
//            }
//
//        }
//        return super.onKeyDown(keyCode, event);
//    }*/
}
