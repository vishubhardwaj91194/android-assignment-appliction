package com.daffodil.assignment.dataholders;

public class ApplicationConstants {
    public static final int VIEW_TYPE_FIRST = 0;
    public static final int VIEW_TYPE_SECOND = 1;
    public static final int VIEW_TYPE_THIRD = 2;
    public static final int VIEW_TYPE_NEXT = 1;

    public static final String JSON_TAG_TITLE = "articletitle";
    public static final String JSON_TAG_AUTHOR = "author";
    public static final String JSON_TAG_NAME = "sciencename";
    public static final String JSON_TAG_DOP = "dop";
    public static final String JSON_TAG_IMAGE = "image";
    public static final String JSON_TAG_FB_COUNT = "fbcount";
    public static final String JSON_TAG_TWITTER_COUNT = "twittercount";
    public static final String JSON_TAG_ID = "articleid";
    public static final String JSON_TAG_HTML = "articlehtml";

    public static final String PASSED_ITEM_ID = "item_id";

    public static final String PASSED_URL = "passed_url";
}
