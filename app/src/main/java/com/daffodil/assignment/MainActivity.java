package com.daffodil.assignment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daffodil.assignment.adapters.MyAdapter;
import com.daffodil.assignment.database.RealmDataManager;
import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.helpers.ArticleHelper;
import com.daffodil.assignment.helpers.HelperMethods;
import com.daffodil.assignment.helpers.JsonUtil;
import com.daffodil.assignment.listeners.Listeners;

import java.io.IOException;
import java.util.ArrayList;

import io.realm.RealmResults;


public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // set add image
        ImageView addImageView = (ImageView) findViewById(R.id.addImageView);
        HelperMethods.setAddImage(addImageView,this);
        initTopToolbar();
        initNavigationDrawer();

        mainFunction();
        //initRecyclerLayout();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void initTopToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_menu_big);
            toolbar.setLogo(R.drawable.toolbar_logo);
            toolbar.setTitle("");
        }
        setSupportActionBar(toolbar);
    }

    private void initNavigationDrawer() {
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {
        };
        // Set the drawer toggle as the DrawerListener
        //  mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (mDrawerLayout != null) {
            mDrawerLayout.addDrawerListener(mDrawerToggle);
        }
    }

    /**
     * <p>this is method to check net  {@link #downloadAndSave3()}</p>
     * @return true if net exists else returns false
     */
    private boolean checkNetExistes() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            // display error
            Toast.makeText(getApplicationContext(), "Error connecting to server. ", Toast.LENGTH_SHORT).show();
            RelativeLayout netNotAvailableLayout = (RelativeLayout) findViewById(R.id.no_net);
            if (netNotAvailableLayout != null) {
                netNotAvailableLayout.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    private boolean checkFirstRun() {
        SharedPreferences runCheck = getSharedPreferences("com.daffodil.assignment",MODE_PRIVATE); //load the preferences
        Boolean hasRun = runCheck.getBoolean("hasRun", false); //see if it's run before, default no
        // hasRun = false;
        if (!hasRun) {
            SharedPreferences settings = getSharedPreferences("com.daffodil.assignment", MODE_PRIVATE);
            SharedPreferences.Editor edit = settings.edit();
            edit.putBoolean("hasRun", true); //set to has run
            edit.apply(); //apply or commit
            return true;
            //code for if this is the first time the app has run
            //return true;
        }
//        else {
            //code if the app HAS run before
            return false;
//        }
    }

//    private void downloadAndSave() {
//        final ArticleDownloadTaskListener listener = new ArticleDownloadTaskListener() {
//            @Override
//            public void onFinish(ArrayList<ArticleRealm> passedArrayList) {
//                ArticleHelper.AddAllArticleCompleted testListener = new ArticleHelper.AddAllArticleCompleted() {
//                    @Override
//                    public void onFinish(ArrayList<ArticleRealm> passedArticles) {
//                        MyAdapter mAdapter = new MyAdapter(passedArticles, getApplicationContext());
//                        mRecyclerView.setAdapter(mAdapter);
//                        // ArticleHelper.exportDB(getApplicationContext());
//                    }
//                };
//                ArticleHelper.addAllArticleDataHelper(passedArrayList, testListener, getApplicationContext());
//            }
//        };
//        new ArticleDownloadTask(MainActivity.this, listener).execute();
//    }

//    private void loadAndDisplay() {
//        ArticleHelper.GetAllArticleCompleted finalListener = new ArticleHelper.GetAllArticleCompleted()
//        {
//            @Override
//            public void onFinish(ArrayList<ArticleRealm> articles) {
//                    MyAdapter mAdapter = new MyAdapter(articles, MainActivity.this);
//                    mRecyclerView.setAdapter(mAdapter);
//            }
//        };
//        ArticleHelper.getAllArticles(finalListener,getApplicationContext());
//    }

    private void mainFunction() {
        // Check if net exists
        if(checkNetExistes()){
            mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
            if (mRecyclerView != null) {
                mRecyclerView.setHasFixedSize(true);
            }
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            if(checkFirstRun()){
                // Fetch data and save it to
                downloadAndSave3();
            }else{
                loadAndDisplay2();

            }

        }
    }

    private void loadAndDisplay2() {
        Listeners.OnGetAllArticles finalListener = new Listeners.OnGetAllArticles()
        {
            @Override
            public void onSuccess(RealmResults results) {
                ArrayList<ArticleRealm> list = new ArrayList<>();
                ArticleRealm obj;
                for(int i=0;i<10;i++){
                    obj = (ArticleRealm)results.get(i);
                    list.add(obj);
//                    RealmDataManager m = new RealmDataManager();
                }
                MyAdapter mAdapter = new MyAdapter(list, MainActivity.this);
                mRecyclerView.setAdapter(mAdapter);
            }
        };
        RealmDataManager dbm = new RealmDataManager();
        dbm.getAllArticles(finalListener);

    }

//    private void downloadAndSave2() {
//        int skip = 0, limit =10;
//        com.android.volley.Response.Listener<String> testListener = new com.android.volley.Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                ArrayList<ArticleRealm> articleList=null;
//                articleList = JsonUtil.jsonDataToArrayList2(response);
//                final ArrayList<ArticleRealm> finalArticleList = articleList;
//                ArticleHelper.AddAllArticleCompleted testListener = new ArticleHelper.AddAllArticleCompleted() {
//                    @Override
//                    public void onFinish(ArrayList<ArticleRealm> passedArticles) {
//                        MyAdapter mAdapter = new MyAdapter(finalArticleList, MainActivity.this);
//                        mRecyclerView.setAdapter(mAdapter);
//                        // ArticleHelper.exportDB(getApplicationContext());
//                    }
//                };
//                ArticleHelper.addAllArticleDataHelper(articleList, testListener, getApplicationContext());
//            }
//        };
//        try {
//            ArticleHelper.runValle(skip,limit,getApplicationContext(),testListener);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * <p>
     *     This method uses volley to download data from net and uses realm to save data to database.
     *     It calls <code>ArticleHelper.runValle</code> to download data with callback.
     *     After that it converts String responce to {@link ArrayList< ArticleRealm >} via {@link JsonUtil}.<code>jsonDataToArrayList2</code>.
     *     It inserys data via {@link RealmDataManager}.<code>addAllArticleHelper</code> to add data to database.
     * </p>
     */
    private void downloadAndSave3() {
        int skip = 0, limit = 10;
        final ProgressDialog progressDialog=new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.downloading_notice));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.show();
        com.android.volley.Response.Listener<String> testListener = new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<ArticleRealm> articleList;
                articleList = JsonUtil.jsonDataToArrayList2(response);
                final ArrayList<ArticleRealm> finalArticleList = articleList;
                ArticleHelper.AddAllArticleCompleted testListener = new ArticleHelper.AddAllArticleCompleted() {
                    @Override
                    public void onFinish(ArrayList<ArticleRealm> passedArticles) {
                        MyAdapter mAdapter = new MyAdapter(finalArticleList, MainActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                        // ArticleHelper.exportDB(getApplicationContext());
                        progressDialog.dismiss();
                    }
                };
                // ArticleHelper.addAllArticleDataHelper(articleList, testListener, getApplicationContext());
                RealmDataManager.addAllArticleDataHelper(articleList, testListener);
            }
        };
        try {
            ArticleHelper.runValle(skip, limit, getApplicationContext(), testListener);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private void loadDataFromVolly() {
//        int skip = 0, limit =10;
//        com.android.volley.Response.Listener<String> testListener = new com.android.volley.Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                ArrayList<ArticleRealm> articleList=null;
//                articleList = JsonUtil.jsonDataToArrayList2(response);
//                MyAdapter mAdapter = new MyAdapter(articleList, getApplicationContext());
//                mRecyclerView.setAdapter(mAdapter);
//            }
//        };
//        try {
//            ArticleHelper.runValle(skip,limit,getApplicationContext(),testListener);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void initRecyclerLayout() {
//        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
//        if (mRecyclerView != null) {
//            mRecyclerView.setHasFixedSize(true);
//        }
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//
//        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//        if (networkInfo != null && networkInfo.isConnected()) {
//            // fetch data
//            final ArticleDownloadTaskListener listener = new ArticleDownloadTaskListener() {
//                @Override
//                public void onFinish(ArrayList<ArticleRealm> passedArticleList) {
//                    MyAdapter passedAdapter = new MyAdapter(passedArticleList, getApplicationContext());
//                    mRecyclerView.setAdapter(passedAdapter);
//                    ArticleHelper.exportDB(getApplicationContext());
//                }
//            };
//            final OptionMenuCallBackInterface callBackInterface = new OptionMenuCallBackInterface() {
//                @Override
//                public void onFinish(ImageView imageView) {
//                    registerForContextMenu(imageView);
//                }
//            };
//
//            /*
//                -- for downloading data from net --
//                new ArticleDownloadTask(this, listener, callBackInterface).execute();
//            */
//            /*
//                -- for downloading data from database --
//             */
//
//            ArticleHelper.GetAllArticleCompleted finalListener = new ArticleHelper.GetAllArticleCompleted()
//            {
//                @Override
//                public void onFinish(ArrayList<ArticleRealm> articles) {
//                    if(articles.size()==0){
//                        new ArticleDownloadTask(MainActivity.this, listener).execute();
//                    }else{
//                        MyAdapter mAdapter = new MyAdapter(articles, getApplicationContext());
//                        mRecyclerView.setAdapter(mAdapter);
//                    }
//                }
//            };
//            ArticleHelper.getAllArticles(finalListener,getApplicationContext());
//
//            // end of code to fetch data from net
//        } else {
//            // display error
//            Toast.makeText(getApplicationContext(), "Error connecting to server. ", Toast.LENGTH_SHORT).show();
//            RelativeLayout netNotAvailableLayout = (RelativeLayout) findViewById(R.id.no_net);
//            if (netNotAvailableLayout != null) {
//                netNotAvailableLayout.setVisibility(View.VISIBLE);
//            }
//        }
//    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == R.id.option_menu_icon){
            getMenuInflater().inflate(R.menu.option_menu, menu);
        }
    }
}