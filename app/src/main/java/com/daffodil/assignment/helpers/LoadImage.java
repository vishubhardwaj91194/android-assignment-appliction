package com.daffodil.assignment.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.daffodil.assignment.R;

import java.io.InputStream;
import java.net.URL;

public class LoadImage extends AsyncTask<String, String, Bitmap> {
    ImageView iv;

    LoadImage(ImageView iv12){
        iv = iv12;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//            iv.setImageResource(R.drawable.default_icon);
    }

    protected Bitmap doInBackground(String... args) {
        Bitmap bitmap = null;
        try {

            bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());


        } catch (Exception e) {
            System.out.println(e);
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap image) {

        if(image == null){
            Log.d("Error1233","Could not load image");
            iv.setImageResource(R.drawable.default_icon);

        }else{
            iv.setImageBitmap(image);

        }
    }
}
