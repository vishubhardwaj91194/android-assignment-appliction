package io.realm;


public interface ArticleRealmRealmProxyInterface {
    public int realmGet$_id();
    public void realmSet$_id(int value);
    public String realmGet$title();
    public void realmSet$title(String value);
    public String realmGet$author();
    public void realmSet$author(String value);
    public String realmGet$scienceName();
    public void realmSet$scienceName(String value);
    public String realmGet$dop();
    public void realmSet$dop(String value);
    public String realmGet$image();
    public void realmSet$image(String value);
    public String realmGet$fbCount();
    public void realmSet$fbCount(String value);
    public String realmGet$twitterCount();
    public void realmSet$twitterCount(String value);
}
