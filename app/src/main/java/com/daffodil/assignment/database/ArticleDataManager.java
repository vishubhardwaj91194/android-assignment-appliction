package com.daffodil.assignment.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.helpers.ArticleHelper;

import java.util.ArrayList;

public class ArticleDataManager {
    private SQLiteDatabase database;
    private DatabaseManager dbHelper;
    private String[] allColumns = { DatabaseManager.ARTICLE_KEY_ID, DatabaseManager.ARTICLE_KEY_TITLE, DatabaseManager.ARTICLE_KEY_AUTHOR, DatabaseManager.ARTICLE_KEY_SCIENCE_NAME, DatabaseManager.ARTICLE_KEY_IMAGE, DatabaseManager.ARTICLE_KEY_DOP, DatabaseManager.ARTICLE_KEY_FB_COUNT, DatabaseManager.ARTICLE_KEY_TWITTER_COUNT };

    ArticleDataManager(Context context) {
        dbHelper = new DatabaseManager(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long insert(ArticleRealm articleRealm){
        ContentValues values = new ContentValues();
        values.put(DatabaseManager.ARTICLE_KEY_TITLE, articleRealm.getTitle());
        values.put(DatabaseManager.ARTICLE_KEY_AUTHOR, articleRealm.getAuthor());
        values.put(DatabaseManager.ARTICLE_KEY_SCIENCE_NAME, articleRealm.getScienceName());
        values.put(DatabaseManager.ARTICLE_KEY_DOP, articleRealm.getDop());
        values.put(DatabaseManager.ARTICLE_KEY_IMAGE, articleRealm.getImage());
        values.put(DatabaseManager.ARTICLE_KEY_FB_COUNT, articleRealm.getFbCount());
        values.put(DatabaseManager.ARTICLE_KEY_TWITTER_COUNT, articleRealm.getTwitterCount());
        return database.insert(DatabaseManager.TABLE_ARTICLE, null, values);
    }

    public ArrayList<ArticleRealm> readAll() {
        ArrayList<ArticleRealm> contacts = new ArrayList<>();
        Cursor cursor = database.query(DatabaseManager.TABLE_ARTICLE, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ArticleRealm comment = cursorToArticle(cursor);
            contacts.add(comment);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        //Log.d("Database path", "readAll: "+database.getPath());
        cursor.close();
        return contacts;
    }

    public ArticleRealm readOne(int id){
        Cursor cursor = database.query(DatabaseManager.TABLE_ARTICLE,
                allColumns, DatabaseManager.ARTICLE_KEY_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        ArticleRealm newComment = cursorToArticle(cursor);
        cursor.close();
        return newComment;
    }

    public void delete(ArticleRealm article) {
        long id = article.get_id();
        database.delete(DatabaseManager.TABLE_ARTICLE, DatabaseManager.ARTICLE_KEY_ID + " = " + id, null);
    }

    private ArticleRealm cursorToArticle(Cursor cursor) {
        ArticleRealm article = new ArticleRealm();
        article.set_id((int) cursor.getLong(0));
        article.setTitle(cursor.getString(1));
        article.setAuthor(cursor.getString(2));
        article.setScienceName(cursor.getString(3));
        article.setDop(cursor.getString(5));
        article.setImage(cursor.getString(4));
        article.setFbCount(cursor.getString(6));
        article.setTwitterCount(cursor.getString(7));
        return article;
    }

    public void addArticle(final ArticleRealm passedArticle, ArticleHelper.AddArticleCompleted passedListener){

        class AddArticleAsyncTask extends AsyncTask<Void,Void,String>{
            ArticleHelper.AddArticleCompleted listener;

            AddArticleAsyncTask(ArticleHelper.AddArticleCompleted passedListener){
                listener = passedListener;
            }

            @Override
            protected String doInBackground(Void... params) {
                passedArticle.set_id((int) insert(passedArticle));
                return ""+passedArticle.get_id();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                listener.onFinish(s);
            }
        }

        (new AddArticleAsyncTask(passedListener)).execute();
    }

    public void getAllArticle(ArticleHelper.GetAllArticleCompleted passedListener){

        class GetAllArticleAsyncTask extends AsyncTask<Void,Void,ArrayList<ArticleRealm>>{
            ArticleHelper.GetAllArticleCompleted listener;

            GetAllArticleAsyncTask(ArticleHelper.GetAllArticleCompleted passedListener){
                listener = passedListener;
            }

            @Override
            protected ArrayList<ArticleRealm> doInBackground(Void... params) {
                return readAll();
            }

            @Override
            protected void onPostExecute(ArrayList<ArticleRealm> arrayList) {
                super.onPostExecute(arrayList);
                listener.onFinish(arrayList);
            }
        }

        (new GetAllArticleAsyncTask(passedListener)).execute();
    }

    public void getArticleForId(ArticleHelper.GetArticleForIdCompleted passedListener, final long id) {

        class GetArticleForIdAsyncTask extends AsyncTask<Void,Void,ArticleRealm>{
            ArticleHelper.GetArticleForIdCompleted listener;

            GetArticleForIdAsyncTask(ArticleHelper.GetArticleForIdCompleted passedListener){
                listener = passedListener;
            }

            @Override
            protected ArticleRealm doInBackground(Void... params) {
                return readOne((int) id);
            }

            //@Override
            protected void onPostExecute(ArticleRealm s) {
                //super.onPostExecute(s);
                listener.onFinish(s);
            }
        }

        (new GetArticleForIdAsyncTask(passedListener)).execute();
    }

    public void addAllArticles(final ArrayList<ArticleRealm> articleArrayList, final ArticleHelper.AddAllArticleCompleted passedListener){
        class AddArticleAsyncTask extends AsyncTask<Void,Void,ArrayList<ArticleRealm>>{
            ArticleHelper.AddAllArticleCompleted listener;

            AddArticleAsyncTask(ArticleHelper.AddAllArticleCompleted passedListener){
                listener = passedListener;
            }

            @Override
            protected ArrayList<ArticleRealm> doInBackground(Void... params) {
                database.beginTransaction();
                for(ArticleRealm oneArticle:articleArrayList){ oneArticle.set_id((int) insert(oneArticle)); }
                database.setTransactionSuccessful();
                database.endTransaction();
                return articleArrayList;
            }

            @Override
            protected void onPostExecute(ArrayList<ArticleRealm> s) {
                super.onPostExecute(s);
                passedListener.onFinish(s);
            }
        }

        (new AddArticleAsyncTask(passedListener)).execute();
    }

}
