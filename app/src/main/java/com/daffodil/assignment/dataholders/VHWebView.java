package com.daffodil.assignment.dataholders;

import com.daffodil.assignment.listeners.OnWebViewClickListener;

public class VHWebView implements VHBaseDetails {
    public String vewHtml;
    public OnWebViewClickListener listener;
    public VHWebView(OnWebViewClickListener listener){
        this.listener = listener;
    }
    public OnWebViewClickListener getListener(){
        return this.listener;
    }
}
