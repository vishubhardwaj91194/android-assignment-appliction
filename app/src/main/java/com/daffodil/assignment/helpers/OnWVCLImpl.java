package com.daffodil.assignment.helpers;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.daffodil.assignment.WebViewActivity;
import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.listeners.OnWebViewClickListener;

public class OnWVCLImpl implements OnWebViewClickListener {
    private Context context;

    public OnWVCLImpl(Context context){
        this.context = context;
    }

    @Override
    public void clickFromDetailScreen(String url) {
        if(!url.contains("youtube")){
            // Toast.makeText(view.getContext(), url, Toast.LENGTH_SHORT).show();
            Intent detailIntent = new Intent(context, WebViewActivity.class);
            detailIntent.putExtra(ApplicationConstants.PASSED_URL,url);
            context.startActivity(detailIntent);
        }else{
            Toast.makeText(context, "Dode", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void clickFromWebViewActivity(String url) {

    }
}
