package com.daffodil.assignment.dataholders;

public class Article {
    public long _id;
    public String title;
    public String author;
    public String scienceName;
    public String dop;
    public String image;
    public String fbCount;
    public String twitterCount;
}
