package com.daffodil.assignment.listeners;

public interface OnWebViewClickListener {
    void clickFromDetailScreen(String url);
    void clickFromWebViewActivity(String url);
}
