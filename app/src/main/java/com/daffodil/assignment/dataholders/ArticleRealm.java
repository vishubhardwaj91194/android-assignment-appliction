package com.daffodil.assignment.dataholders;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class ArticleRealm extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;
    @Required
    private String title;
    @Required
    private String author;
    @Required
    private String scienceName;
    @Required
    private String dop;
    @Required
    private String image;
    @Required
    private String fbCount;
    @Required
    private String twitterCount;

    // Getters
    public int get_id(){ return this._id; }
    public String getTitle(){ return this.title; }
    public String getAuthor(){ return this.author; }
    public String getScienceName(){ return this.scienceName; }
    public String getDop(){ return this.dop; }
    public String getImage(){ return this.image; }
    public String getFbCount(){ return this.fbCount; }
    public String getTwitterCount(){ return this.twitterCount; }

    // Setters
    public void set_id(int x){ this._id = x; }
    public void setTitle(String x){ this.title = x; }
    public void setAuthor(String x){ this.author = x; }
    public void setScienceName(String x){ this.scienceName = x; }
    public void setDop(String x){ this.dop = x; }
    public void setImage(String x){ this.image = x; }
    public void setFbCount(String x){ this.fbCount = x; }
    public void setTwitterCount(String x){ this.twitterCount = x; }
}
