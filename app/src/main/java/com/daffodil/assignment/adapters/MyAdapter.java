package com.daffodil.assignment.adapters;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.R;
import com.daffodil.assignment.viewholders.ViewHolder;
import com.daffodil.assignment.helpers.HelperMethods;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<ViewHolder> {
    private ArrayList<ArticleRealm> arrayListOfArticles;
    private int length;
    private Context applicationContext;

    public MyAdapter(ArrayList<ArticleRealm> passedArrayList, Context passedApplicationContext){
        this.arrayListOfArticles = passedArrayList;
        length = passedArrayList.size();
        applicationContext = passedApplicationContext;
        //notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View returnedView;
        if(viewType == ApplicationConstants.VIEW_TYPE_FIRST){
            returnedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.first_post, parent, false);
            returnedView.findViewById(R.id.add_to_my_box).setVisibility(View.INVISIBLE);
        }else{
            returnedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.next_post, parent, false);
        }
//        returnedView.setOnClickListener(this);
        return new ViewHolder(returnedView, true);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.title.setText(arrayListOfArticles.get(position).getTitle());
        holder.tag11.setText(arrayListOfArticles.get(position).getAuthor());
        holder.tag12.setText(arrayListOfArticles.get(position).getScienceName());
        holder.date1.setText(arrayListOfArticles.get(position).getDop());
        holder.sm_1.setText(arrayListOfArticles.get(position).getFbCount());
        holder.sm_2.setText(arrayListOfArticles.get(position).getTwitterCount());

        // set item id in view holde
        String id = String.valueOf(arrayListOfArticles.get(position).get_id());
        holder.itemId.setText(id);

        //Picasso.with(applicationContext).load(arrayListOfArticles.get(position).image).into(holder.image_view);
        HelperMethods.picassoLoadImage(holder.image_view,arrayListOfArticles.get(position).getImage(),applicationContext);
        holder.optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu p = new PopupMenu(applicationContext, v);
                p.getMenuInflater().inflate(R.menu.option_menu,p.getMenu());
                p.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return length;
    }

    @Override
    public int getItemViewType(int position) {
         if(position == ApplicationConstants.VIEW_TYPE_FIRST)
             return ApplicationConstants.VIEW_TYPE_FIRST;
        return ApplicationConstants.VIEW_TYPE_NEXT;
    }

    /*@Override
    public void onClick(View v) {
        TextView itemIdTextView = (TextView) v.findViewById(R.id.item_id);
        int id = Integer.parseInt(itemIdTextView.getText().toString());

        // Toast.makeText(applicationContext, "item clicked "+id,Toast.LENGTH_SHORT).show();
        Intent detailIntent = new Intent(applicationContext, DetailScreen.class);
        detailIntent.putExtra(ApplicationConstants.PASSED_ITEM_ID,id);
        applicationContext.startActivity(detailIntent);
    }*/
}
