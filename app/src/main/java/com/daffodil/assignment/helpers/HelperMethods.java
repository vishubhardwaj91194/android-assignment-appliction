package com.daffodil.assignment.helpers;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HelperMethods {

    public static void picassoLoadImage(final ImageView imageView, final String url, final Context context){

//        final ProgressDialog progressDialog=new ProgressDialog(context);
//        progressDialog.setMessage("Downloading image");
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setIndeterminate(false);
//        //progressDialog.setProgress(0);
//        progressDialog.show();
        //Picasso.with(context).load(url).tag("TAH").into(imageView);

        Picasso.with(context)
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        //sadasd
                        Log.e("Picasso","cache");
//                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError() {
                        //Try again online if cache failed
                        Picasso.with(context)
                                .load(url)
                                //.error(R.drawable.header)
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        //asdd
                                        Log.e("Picasso","net");
//                                        progressDialog.dismiss();
                                    }

                                    @Override
                                    public void onError() {
                                        Log.e("Picasso","Could not fetch image");
                                    }
                                });
                    }
                });
    }

    public static void setAddImage(ImageView passedImageView, Context context){
        String addUrl = UrlHelper.getAdvertisementUrl();
        //Picasso.with(context).load(addUrl).into(passedImageView);
        picassoLoadImage(passedImageView,addUrl,context);
    }

    public static String dateConverter(String oldDate, int position){
        oldDate = oldDate.replace('T','-');
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm:SSS");
        SimpleDateFormat fd = new SimpleDateFormat("dd");
        SimpleDateFormat fr = new SimpleDateFormat("MMM, yyyy");
        String[] extension = {"st","nd","rd","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","th","st","nd","rd","th","th","th","th","th","th","th","st"};
        int d;
        String dateInString;

        try {
            Date date = formatter.parse(oldDate);
            d = Integer.parseInt(fd.format(date));
            //System.out.println(date);
            dateInString = d+extension[(d-1)]+" "+fr.format(date);
            //System.out.println(dateInString);
            //Log.e("Error",dateInString);
            oldDate = dateInString;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(position==0){oldDate="| "+oldDate;}
        return oldDate;
    }
}
