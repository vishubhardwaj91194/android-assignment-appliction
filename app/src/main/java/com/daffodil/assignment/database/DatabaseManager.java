package com.daffodil.assignment.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "ArticleDatabase";

    // Article table name
    public static final String TABLE_ARTICLE = "articleTable";

    // Article Table Columns names
    public static final String ARTICLE_KEY_ID = "id";
    public static final String ARTICLE_KEY_TITLE = "title";
    public static final String ARTICLE_KEY_AUTHOR = "author";
    public static final String ARTICLE_KEY_SCIENCE_NAME = "scienceName";
    public static final String ARTICLE_KEY_DOP = "dop";
    public static final String ARTICLE_KEY_IMAGE = "image";
    public static final String ARTICLE_KEY_FB_COUNT = "fbCount";
    public static final String ARTICLE_KEY_TWITTER_COUNT = "twitterCount";


    public DatabaseManager(Context context) {
        /*
            For creating database in external storage
         */
        //super(context, Environment.getExternalStorageDirectory()+ File.separator+"Assignment"+ File.separator+DATABASE_NAME, null, DATABASE_VERSION);
        /*
            For creating database in default /data/data/com.daffodil.assignment/database location
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

         */
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create article table
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ARTICLE + "(" + ARTICLE_KEY_ID + " INTEGER PRIMARY KEY," + ARTICLE_KEY_TITLE + " TEXT," + ARTICLE_KEY_AUTHOR + " TEXT," + ARTICLE_KEY_SCIENCE_NAME + " TEXT," + ARTICLE_KEY_DOP + " TEXT," + ARTICLE_KEY_IMAGE + " TEXT," + ARTICLE_KEY_FB_COUNT + " TEXT," + ARTICLE_KEY_TWITTER_COUNT + " TEXT" +")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        // Create tables again
        onCreate(db);
    }
}
