package com.daffodil.assignment.database;

import android.util.Log;

import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.helpers.ArticleHelper;
import com.daffodil.assignment.listeners.Listeners;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class RealmDataManager {

    public void addArticle(final ArticleRealm article, final Listeners.OnAddArticle callback) {
        //Realm realm = Realm.getInstance(MyApplication.getInstance());
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ArticleRealm a = realm.createObject(ArticleRealm.class);
                // realm.allObjects(Article.class).sort("_id",false);
                /*
                // for setting id to article as auto increment. we dont neeed it now since id comes from net
                    RealmResults<ArticleRealm> results = realm.where(ArticleRealm.class).findAllSorted("_id", Sort.DESCENDING);
                    if (results.size() == 0) {
                        a.set_id(1);
                    } else {
                        a.set_id(results.first().get_id() + 1);
                    }
                */
                // new code
                a.set_id(article.get_id());
                a.setTitle(article.getTitle());
                a.setAuthor(article.getAuthor());
                a.setScienceName(article.getScienceName());
                a.setDop(article.getDop());
                a.setImage(article.getImage());
                a.setFbCount(article.getFbCount());
                a.setTwitterCount(article.getTwitterCount());
                // article = a;
                // we dont need it now as article already had id from net.
                // article.set_id(a.get_id());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (callback != null) {
                    callback.onSuccess(article);
                }
            }
        });
        //realm.beginTransaction();
        //realm.commitTransaction();
    }


    public void updateArticle(final ArticleRealm article, final Listeners.OnAddArticle callback) {
        Realm realm = Realm.getDefaultInstance();
        final RealmObject[] a = new RealmObject[1];

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                a[0] = realm.copyToRealmOrUpdate(article);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (callback != null) {
                    callback.onSuccess((ArticleRealm) a[0]);
                }
            }
        });
    }

    public void getArticleById(final int id, final Listeners.OnGetArticle callback) {
        Realm realm = Realm.getDefaultInstance();
        final ArticleRealm[] result = new ArticleRealm[1];
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                result[0] = realm.where(ArticleRealm.class).equalTo("_id", id).findFirst();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (callback != null) {
                    callback.onSuccess(result[0]);
                }
            }
        });
    }

    public void getAllArticles(final Listeners.OnGetAllArticles callback) {
        Realm realm = Realm.getDefaultInstance();
        //final RealmQuery[] query = new RealmQuery[1];
        //final RealmResults<ArticleRealm>[] results = new RealmResults[1];

        new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                final RealmResults<ArticleRealm> results1 = realm.where(ArticleRealm.class).findAllAsync();
                results1.addChangeListener(new RealmChangeListener<RealmResults<ArticleRealm>>() {
                    @Override
                    public void onChange(RealmResults<ArticleRealm> element) {
                        if (callback != null) {
                            callback.onSuccess(results1);
                        }
                    }
                });

            }
        }.execute(realm);


//        realm.executeTransactionAsync(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                query[0] = realm.where(ArticleRealm.class);
//
//                results[0] = query[0].findAllAsync();
//            }
//        }, new Realm.Transaction.OnSuccess() {
//            @Override
//            public void onSuccess() {
//
//            }
//        });
    }

    public void deleteArticle(final int position, final Listeners.OnDeleteArticle callback) {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //RealmQuery query = realm.where(Article.class).equalTo("_id",position);
                RealmResults<ArticleRealm> query = realm.where(ArticleRealm.class).equalTo("_id", position).findAll();
                query.first().deleteFromRealm();
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (callback != null) {
                    callback.onSuccess();
                }
            }
        });
    }


    public static void addAllArticleDataHelper(final ArrayList<ArticleRealm> articleList, final ArticleHelper.AddAllArticleCompleted testListener) {
        // ArrayList<Article> aricleRealmList = new ArrayList<>();
        RealmDataManager dbManager = new RealmDataManager();
        final int length = articleList.size();
        final int[] i = {0};
        for (final ArticleRealm testArticleRealm : articleList) {
            //i[0]++;
            Listeners.OnAddArticle listener = new Listeners.OnAddArticle() {
                @Override
                public void onSuccess(ArticleRealm a) {
                    Log.d("ArticleAdded", a.toString());
                    if (i[0] == (length - 1)) {
                        testListener.onFinish(articleList);
                    }
                    i[0]++;
                }
            };
            dbManager.addArticle(testArticleRealm, listener);
            // aricleRealmList.add(testArticle);
        }
    }

}
