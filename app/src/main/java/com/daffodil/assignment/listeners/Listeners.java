package com.daffodil.assignment.listeners;

import com.daffodil.assignment.dataholders.ArticleRealm;

import java.util.ArrayList;

import io.realm.RealmResults;

public class Listeners {
    public interface OnAddArticle {
        void onSuccess(ArticleRealm a);
    }
    public interface OnGetAllArticles {
        void onSuccess(RealmResults results);
    }
    public interface OnGetArticle {
        void onSuccess(ArticleRealm result);
    }
    public interface OnDeleteArticle {
        void onSuccess();
    }

    public interface AddAllArticleCompleted{
        void onFinish(ArrayList<ArticleRealm> list);
    }
}
