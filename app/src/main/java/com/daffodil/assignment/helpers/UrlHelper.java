package com.daffodil.assignment.helpers;

public class UrlHelper {
    public static String baseUrl = "http://knowlarity.izofy.com/izofyapi/api/";
    public static String getAdvertisementUrl(){
        return "http://www.groceryrebateapps.com/wp-content/uploads/2015/02/groceryrebateapps-logo01.png";
    }
    public static String getDetailArticleUrl(int item_id){
        return baseUrl + "article/get/" + item_id + "?apiKey=fac56a890f51471b9be8600e550c262adf64efac93cc4d22a4c84b84801207f7";
    }
    public static String getSimilarArticleUrl(int item_id){
        return  baseUrl + "article/getsimilararticle/" + item_id + "?skip=0&limit=2&apikey=fac56a890f51471b9be8600e550c262adf64efac93cc4d22a4c84b84801207f7";
    }
}
