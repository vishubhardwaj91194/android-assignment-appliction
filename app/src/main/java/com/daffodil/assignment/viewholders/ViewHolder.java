package com.daffodil.assignment.viewholders;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.DetailScreen;
import com.daffodil.assignment.R;
import com.daffodil.assignment.dataholders.VHDetailHeader;
import com.daffodil.assignment.dataholders.VHSimilarItems;
import com.daffodil.assignment.helpers.HelperMethods;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView title, tag11, tag12, date1, sm_1, sm_2;
    public ImageView image_view;
    public ImageView optionMenu;
    public TextView itemId;
    public Context context;

    public ViewHolder(View itemView, boolean isClickRequired) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.post_1_title);
        tag11 = (TextView) itemView.findViewById(R.id.post_1_tag_1);
        tag12 = (TextView) itemView.findViewById(R.id.post_1_tag_2);
        date1 = (TextView) itemView.findViewById(R.id.post_1_date);
        sm_1 = (TextView) itemView.findViewById(R.id.post_1_sm_1);
        sm_2 = (TextView) itemView.findViewById(R.id.post_1_sm_2);
        image_view = (ImageView) itemView.findViewById(R.id.post_1_image);
        optionMenu = (ImageView) itemView.findViewById(R.id.option_menu_icon);
        itemId = (TextView) itemView.findViewById(R.id.item_id);
        context = itemView.getContext();
        if(isClickRequired){
            itemView.setOnClickListener(this);
        }
        image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setType("images/*");
                v.buildDrawingCache();
                Bitmap image= v.getDrawingCache();

                Bundle extras = new Bundle();
                extras.putParcelable("imagebitmap", image);
                intent.putExtras(extras);


               /* Bundle extras = getIntent().getExtras();
                Bitmap bmp = (Bitmap) extras.getParcelable("imagebitmap");

                image.setImageBitmap(bmp );*/
                //intent.putExtra("data",);
                context.startActivity(Intent.createChooser(intent,"View picture"));
            }
        });
    }

    public void setViewForHeader(VHDetailHeader detailHeader){
        this.title.setText(detailHeader.title);
        this.tag11.setText(detailHeader.author);
        this.tag12.setText(detailHeader.scienceName);
        this.date1.setText(detailHeader.dop);
        this.sm_1.setText(detailHeader.fbCount);
        this.sm_2.setText(detailHeader.twitterCount);

        // set item id in view holde
        String id = String.valueOf(detailHeader._id);
        this.itemId.setText(id);

        HelperMethods.picassoLoadImage(this.image_view,detailHeader.image,context);
    }

    public void setViewForSimilar(VHSimilarItems detailHeader){
        this.title.setText(detailHeader.title);
        this.tag11.setText(detailHeader.author);
        this.tag12.setText(detailHeader.scienceName);
        this.date1.setText(detailHeader.dop);
        this.sm_1.setText(detailHeader.fbCount);
        this.sm_2.setText(detailHeader.twitterCount);

        // set item id in view holde
        String id = String.valueOf(detailHeader._id);
        this.itemId.setText(id);

        HelperMethods.picassoLoadImage(this.image_view,detailHeader.image,context);
    }

    @Override
    public void onClick(View v) {
        TextView itemIdTextView = (TextView) v.findViewById(R.id.item_id);
        int id = Integer.parseInt(itemIdTextView.getText().toString());

        // Toast.makeText(applicationContext, "item clicked "+id,Toast.LENGTH_SHORT).show();
        Intent detailIntent = new Intent(context, DetailScreen.class);
        detailIntent.putExtra(ApplicationConstants.PASSED_ITEM_ID,id);
        context.startActivity(detailIntent);
    }
}
