package com.daffodil.assignment.listeners;

public interface DetilsDownloadListener {
    void onSuccess(String r1, String r2);
    void onDetailArticleReceived(String r1);
    void onSimilarArticlesReceived(String r2);
    void onFailure();
}
