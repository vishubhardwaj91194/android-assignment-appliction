package com.daffodil.assignment.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.daffodil.assignment.R;
import com.daffodil.assignment.listeners.OnWebViewClickListener;

public class ViewHolderWebView extends RecyclerView.ViewHolder  {
    public WebView webView;
    public Context context;
    public OnWebViewClickListener listener;

    public ViewHolderWebView(View itemView) {
        super(itemView);
        webView = (WebView) itemView.findViewById(R.id.web_view_layout);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0"); // linux desktop mozilla user agent
        context = itemView.getContext();

        // test code
        WebViewClient webviewclient = new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                return super.shouldOverrideUrlLoading(view, url);
                /*
                    if(!url.contains("youtube")){
                        // Toast.makeText(view.getContext(), url, Toast.LENGTH_SHORT).show();
                        Intent detailIntent = new Intent(context, WebViewActivity.class);
                        detailIntent.putExtra(ApplicationConstants.PASSED_URL,url);
                        context.startActivity(detailIntent);
                    }else{
                        Toast.makeText(view.getContext(), "Dode", Toast.LENGTH_SHORT).show();
                    }
                */
                listener.clickFromDetailScreen(url);
                return true;
            }

        };
        webView.setWebViewClient(webviewclient);
    }

    public void setListener(OnWebViewClickListener listener){
        this.listener = listener;
    }
}