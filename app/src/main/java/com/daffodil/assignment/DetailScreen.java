package com.daffodil.assignment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.daffodil.assignment.adapters.SecondAdapter;
import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.dataholders.VHAdvertisement;
import com.daffodil.assignment.dataholders.VHBaseDetails;
import com.daffodil.assignment.helpers.ArticleHelper;
import com.daffodil.assignment.helpers.JsonUtil;
import com.daffodil.assignment.helpers.OnWVCLImpl;
import com.daffodil.assignment.helpers.UrlHelper;
import com.daffodil.assignment.listeners.DetilsDownloadListener;

import java.util.ArrayList;

public class DetailScreen extends AppCompatActivity {
    private int item_id;
    private Toolbar toolbar;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_screen);

        Intent passedIntent = getIntent();
        item_id = passedIntent.getIntExtra(ApplicationConstants.PASSED_ITEM_ID,0);
        // ((TextView) findViewById(R.id.textView4)).setText(String.valueOf(item_id));

        initToolbar();

        downloadDetailScreen();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            // toolbar.setLogo(R.drawable.toolbar_logo);
            toolbar.setTitle("");
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    finish();
//                }
//            });
        }
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.detail_recycler_view);
        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
        }

    }

    private void downloadDetailScreen(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.downloading_notice));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        // init recycler view
        initRecyclerView();

        final ArrayList<VHBaseDetails> detailsArrayList = new ArrayList<>(3);
        final SecondAdapter mAdapter = new SecondAdapter(detailsArrayList, this);
        mRecyclerView.setAdapter(mAdapter);

        DetilsDownloadListener listener = new DetilsDownloadListener() {
            boolean headerReceived = false;
            @Override
            public void onSuccess(String detailHeaderJsonString, String similarArticleJsonString) {
                /*System.out.print(detailHeaderJsonString+"\n"+similarArticleJsonString);
                progressDialog.dismiss();
                ArrayList<VHBaseDetails> detailsArrayList = new ArrayList<>();
                detailsArrayList.add(JsonUtil.getMainArticle(detailHeaderJsonString));
                detailsArrayList.add(JsonUtil.getDetail(detailHeaderJsonString));

                VHAdvertisement advertisement = new VHAdvertisement();
                advertisement.advertisementUrl = UrlHelper.getAdvertisementUrl();
                detailsArrayList.add(advertisement);
                ArrayList<VHSimilarItems> similarItemList = JsonUtil.getSimilarItemsList(similarArticleJsonString);
//                int i = 3;
//                for(VHSimilarItems testItem:similarItemList){
                detailsArrayList.addAll(similarItemList);
//                    i++;
//                }
                mRecyclerView.setAdapter(mAdapter[0]);*/
            }

            @Override
            public void onDetailArticleReceived(String detailHeaderJsonString) {
                progressDialog.dismiss();
                VHAdvertisement advertisement = new VHAdvertisement();
                advertisement.advertisementUrl = UrlHelper.getAdvertisementUrl();
                //mAdapter[0].modifyDetailsHeader(,,);

                OnWVCLImpl listener1 = new OnWVCLImpl(DetailScreen.this);

                detailsArrayList.add(ApplicationConstants.VIEW_TYPE_FIRST, JsonUtil.getMainArticle(detailHeaderJsonString));
                detailsArrayList.add(ApplicationConstants.VIEW_TYPE_SECOND, JsonUtil.getDetail(detailHeaderJsonString,listener1));
                detailsArrayList.add(ApplicationConstants.VIEW_TYPE_THIRD, advertisement);
                headerReceived = true;
                mAdapter.setDetailsArrayList(detailsArrayList);


                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onSimilarArticlesReceived(String similarArticleJsonString) {
                progressDialog.dismiss();
                //mAdapter[0].modifySimilarArticle();
               /* int i=3;
                for(VHSimilarItems item:JsonUtil.getSimilarItemsList(similarArticleJsonString)){
                    detailsArrayList.add(i,item);
                    i++;
                }*/
//                detailsArrayList.addAll(JsonUtil.getSimilarItemsList(similarArticleJsonString));
                detailsArrayList.addAll(JsonUtil.getSimilarItemsList(similarArticleJsonString));
                if(headerReceived){
                    mAdapter.setDetailsArrayList(detailsArrayList);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure() {
                progressDialog.dismiss();
                finish();
                ErrorReceivingDataDialog errorDialog = new ErrorReceivingDataDialog();
                errorDialog.show(getFragmentManager(), getString(R.string.notice));
            }
        };

        ArticleHelper.fetchDetilData(item_id, listener, getApplicationContext());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
