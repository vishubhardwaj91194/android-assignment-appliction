package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmFieldType;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArticleRealmRealmProxy extends com.daffodil.assignment.dataholders.ArticleRealm
    implements RealmObjectProxy, ArticleRealmRealmProxyInterface {

    static final class ArticleRealmColumnInfo extends ColumnInfo {

        public final long _idIndex;
        public final long titleIndex;
        public final long authorIndex;
        public final long scienceNameIndex;
        public final long dopIndex;
        public final long imageIndex;
        public final long fbCountIndex;
        public final long twitterCountIndex;

        ArticleRealmColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(8);
            this._idIndex = getValidColumnIndex(path, table, "ArticleRealm", "_id");
            indicesMap.put("_id", this._idIndex);

            this.titleIndex = getValidColumnIndex(path, table, "ArticleRealm", "title");
            indicesMap.put("title", this.titleIndex);

            this.authorIndex = getValidColumnIndex(path, table, "ArticleRealm", "author");
            indicesMap.put("author", this.authorIndex);

            this.scienceNameIndex = getValidColumnIndex(path, table, "ArticleRealm", "scienceName");
            indicesMap.put("scienceName", this.scienceNameIndex);

            this.dopIndex = getValidColumnIndex(path, table, "ArticleRealm", "dop");
            indicesMap.put("dop", this.dopIndex);

            this.imageIndex = getValidColumnIndex(path, table, "ArticleRealm", "image");
            indicesMap.put("image", this.imageIndex);

            this.fbCountIndex = getValidColumnIndex(path, table, "ArticleRealm", "fbCount");
            indicesMap.put("fbCount", this.fbCountIndex);

            this.twitterCountIndex = getValidColumnIndex(path, table, "ArticleRealm", "twitterCount");
            indicesMap.put("twitterCount", this.twitterCountIndex);

            setIndicesMap(indicesMap);
        }
    }

    private final ArticleRealmColumnInfo columnInfo;
    private final ProxyState proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("_id");
        fieldNames.add("title");
        fieldNames.add("author");
        fieldNames.add("scienceName");
        fieldNames.add("dop");
        fieldNames.add("image");
        fieldNames.add("fbCount");
        fieldNames.add("twitterCount");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    ArticleRealmRealmProxy(ColumnInfo columnInfo) {
        this.columnInfo = (ArticleRealmColumnInfo) columnInfo;
        this.proxyState = new ProxyState(com.daffodil.assignment.dataholders.ArticleRealm.class, this);
    }

    @SuppressWarnings("cast")
    public int realmGet$_id() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo._idIndex);
    }

    public void realmSet$_id(int value) {
        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo._idIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$title() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.titleIndex);
    }

    public void realmSet$title(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field title to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.titleIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$author() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.authorIndex);
    }

    public void realmSet$author(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field author to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.authorIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$scienceName() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.scienceNameIndex);
    }

    public void realmSet$scienceName(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field scienceName to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.scienceNameIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$dop() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.dopIndex);
    }

    public void realmSet$dop(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field dop to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.dopIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$image() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.imageIndex);
    }

    public void realmSet$image(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field image to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.imageIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$fbCount() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.fbCountIndex);
    }

    public void realmSet$fbCount(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field fbCount to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.fbCountIndex, value);
    }

    @SuppressWarnings("cast")
    public String realmGet$twitterCount() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.twitterCountIndex);
    }

    public void realmSet$twitterCount(String value) {
        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field twitterCount to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.twitterCountIndex, value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_ArticleRealm")) {
            Table table = transaction.getTable("class_ArticleRealm");
            table.addColumn(RealmFieldType.INTEGER, "_id", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "title", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "author", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "scienceName", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "dop", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "image", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "fbCount", Table.NOT_NULLABLE);
            table.addColumn(RealmFieldType.STRING, "twitterCount", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("_id"));
            table.setPrimaryKey("_id");
            return table;
        }
        return transaction.getTable("class_ArticleRealm");
    }

    public static ArticleRealmColumnInfo validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_ArticleRealm")) {
            Table table = transaction.getTable("class_ArticleRealm");
            if (table.getColumnCount() != 8) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 8 but was " + table.getColumnCount());
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < 8; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final ArticleRealmColumnInfo columnInfo = new ArticleRealmColumnInfo(transaction.getPath(), table);

            if (!columnTypes.containsKey("_id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field '_id' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("_id") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field '_id' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo._idIndex) && table.findFirstNull(columnInfo._idIndex) != TableOrView.NO_MATCH) {
                throw new IllegalStateException("Cannot migrate an object with null value in field '_id'. Either maintain the same type for primary key field '_id', or remove the object with null value before migration.");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("_id")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field '_id' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("_id"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field '_id' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            if (!columnTypes.containsKey("title")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'title' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("title") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'title' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.titleIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'title' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'title' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("author")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'author' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("author") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'author' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.authorIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'author' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'author' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("scienceName")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'scienceName' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("scienceName") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'scienceName' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.scienceNameIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'scienceName' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'scienceName' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("dop")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'dop' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("dop") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'dop' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.dopIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'dop' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'dop' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("image")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'image' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("image") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'image' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.imageIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'image' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'image' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("fbCount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'fbCount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("fbCount") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'fbCount' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.fbCountIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'fbCount' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'fbCount' or migrate using RealmObjectSchema.setNullable().");
            }
            if (!columnTypes.containsKey("twitterCount")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'twitterCount' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("twitterCount") != RealmFieldType.STRING) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'String' for field 'twitterCount' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.twitterCountIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'twitterCount' does support null values in the existing Realm file. Remove @Required or @PrimaryKey from field 'twitterCount' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The ArticleRealm class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_ArticleRealm";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.daffodil.assignment.dataholders.ArticleRealm createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        com.daffodil.assignment.dataholders.ArticleRealm obj = null;
        if (update) {
            Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = TableOrView.NO_MATCH;
            if (!json.isNull("_id")) {
                rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("_id"));
            }
            if (rowIndex != TableOrView.NO_MATCH) {
                obj = new io.realm.ArticleRealmRealmProxy(realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class));
                ((RealmObjectProxy)obj).realmGet$proxyState().setRealm$realm(realm);
                ((RealmObjectProxy)obj).realmGet$proxyState().setRow$realm(table.getUncheckedRow(rowIndex));
            }
        }
        if (obj == null) {
            if (json.has("_id")) {
                if (json.isNull("_id")) {
                    obj = (io.realm.ArticleRealmRealmProxy) realm.createObject(com.daffodil.assignment.dataholders.ArticleRealm.class, null);
                } else {
                    obj = (io.realm.ArticleRealmRealmProxy) realm.createObject(com.daffodil.assignment.dataholders.ArticleRealm.class, json.getInt("_id"));
                }
            } else {
                obj = (io.realm.ArticleRealmRealmProxy) realm.createObject(com.daffodil.assignment.dataholders.ArticleRealm.class);
            }
        }
        if (json.has("_id")) {
            if (json.isNull("_id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field _id to null.");
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$_id((int) json.getInt("_id"));
            }
        }
        if (json.has("title")) {
            if (json.isNull("title")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$title(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$title((String) json.getString("title"));
            }
        }
        if (json.has("author")) {
            if (json.isNull("author")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$author(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$author((String) json.getString("author"));
            }
        }
        if (json.has("scienceName")) {
            if (json.isNull("scienceName")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$scienceName(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$scienceName((String) json.getString("scienceName"));
            }
        }
        if (json.has("dop")) {
            if (json.isNull("dop")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$dop(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$dop((String) json.getString("dop"));
            }
        }
        if (json.has("image")) {
            if (json.isNull("image")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$image(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$image((String) json.getString("image"));
            }
        }
        if (json.has("fbCount")) {
            if (json.isNull("fbCount")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$fbCount(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$fbCount((String) json.getString("fbCount"));
            }
        }
        if (json.has("twitterCount")) {
            if (json.isNull("twitterCount")) {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$twitterCount(null);
            } else {
                ((ArticleRealmRealmProxyInterface) obj).realmSet$twitterCount((String) json.getString("twitterCount"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    public static com.daffodil.assignment.dataholders.ArticleRealm createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.daffodil.assignment.dataholders.ArticleRealm obj = realm.createObject(com.daffodil.assignment.dataholders.ArticleRealm.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("_id")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field _id to null.");
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$_id((int) reader.nextInt());
                }
            } else if (name.equals("title")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$title(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$title((String) reader.nextString());
                }
            } else if (name.equals("author")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$author(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$author((String) reader.nextString());
                }
            } else if (name.equals("scienceName")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$scienceName(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$scienceName((String) reader.nextString());
                }
            } else if (name.equals("dop")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$dop(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$dop((String) reader.nextString());
                }
            } else if (name.equals("image")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$image(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$image((String) reader.nextString());
                }
            } else if (name.equals("fbCount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$fbCount(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$fbCount((String) reader.nextString());
                }
            } else if (name.equals("twitterCount")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$twitterCount(null);
                } else {
                    ((ArticleRealmRealmProxyInterface) obj).realmSet$twitterCount((String) reader.nextString());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static com.daffodil.assignment.dataholders.ArticleRealm copyOrUpdate(Realm realm, com.daffodil.assignment.dataholders.ArticleRealm object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.daffodil.assignment.dataholders.ArticleRealm) cachedRealmObject;
        } else {
            com.daffodil.assignment.dataholders.ArticleRealm realmObject = null;
            boolean canUpdate = update;
            if (canUpdate) {
                Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
                long pkColumnIndex = table.getPrimaryKey();
                long rowIndex = table.findFirstLong(pkColumnIndex, ((ArticleRealmRealmProxyInterface) object).realmGet$_id());
                if (rowIndex != TableOrView.NO_MATCH) {
                    realmObject = new io.realm.ArticleRealmRealmProxy(realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class));
                    ((RealmObjectProxy)realmObject).realmGet$proxyState().setRealm$realm(realm);
                    ((RealmObjectProxy)realmObject).realmGet$proxyState().setRow$realm(table.getUncheckedRow(rowIndex));
                    cache.put(object, (RealmObjectProxy) realmObject);
                } else {
                    canUpdate = false;
                }
            }

            if (canUpdate) {
                return update(realm, realmObject, object, cache);
            } else {
                return copy(realm, object, update, cache);
            }
        }
    }

    public static com.daffodil.assignment.dataholders.ArticleRealm copy(Realm realm, com.daffodil.assignment.dataholders.ArticleRealm newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.daffodil.assignment.dataholders.ArticleRealm) cachedRealmObject;
        } else {
            com.daffodil.assignment.dataholders.ArticleRealm realmObject = realm.createObject(com.daffodil.assignment.dataholders.ArticleRealm.class, ((ArticleRealmRealmProxyInterface) newObject).realmGet$_id());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$_id(((ArticleRealmRealmProxyInterface) newObject).realmGet$_id());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$title(((ArticleRealmRealmProxyInterface) newObject).realmGet$title());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$author(((ArticleRealmRealmProxyInterface) newObject).realmGet$author());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$scienceName(((ArticleRealmRealmProxyInterface) newObject).realmGet$scienceName());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$dop(((ArticleRealmRealmProxyInterface) newObject).realmGet$dop());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$image(((ArticleRealmRealmProxyInterface) newObject).realmGet$image());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$fbCount(((ArticleRealmRealmProxyInterface) newObject).realmGet$fbCount());
            ((ArticleRealmRealmProxyInterface) realmObject).realmSet$twitterCount(((ArticleRealmRealmProxyInterface) newObject).realmGet$twitterCount());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.daffodil.assignment.dataholders.ArticleRealm object, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long tableNativePtr = table.getNativeTablePointer();
        ArticleRealmColumnInfo columnInfo = (ArticleRealmColumnInfo) realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo._idIndex, rowIndex, ((ArticleRealmRealmProxyInterface)object).realmGet$_id());
        String realmGet$title = ((ArticleRealmRealmProxyInterface)object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title);
        }
        String realmGet$author = ((ArticleRealmRealmProxyInterface)object).realmGet$author();
        if (realmGet$author != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.authorIndex, rowIndex, realmGet$author);
        }
        String realmGet$scienceName = ((ArticleRealmRealmProxyInterface)object).realmGet$scienceName();
        if (realmGet$scienceName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.scienceNameIndex, rowIndex, realmGet$scienceName);
        }
        String realmGet$dop = ((ArticleRealmRealmProxyInterface)object).realmGet$dop();
        if (realmGet$dop != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dopIndex, rowIndex, realmGet$dop);
        }
        String realmGet$image = ((ArticleRealmRealmProxyInterface)object).realmGet$image();
        if (realmGet$image != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image);
        }
        String realmGet$fbCount = ((ArticleRealmRealmProxyInterface)object).realmGet$fbCount();
        if (realmGet$fbCount != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fbCountIndex, rowIndex, realmGet$fbCount);
        }
        String realmGet$twitterCount = ((ArticleRealmRealmProxyInterface)object).realmGet$twitterCount();
        if (realmGet$twitterCount != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.twitterCountIndex, rowIndex, realmGet$twitterCount);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long tableNativePtr = table.getNativeTablePointer();
        ArticleRealmColumnInfo columnInfo = (ArticleRealmColumnInfo) realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class);
        com.daffodil.assignment.dataholders.ArticleRealm object = null;
        while (objects.hasNext()) {
            object = (com.daffodil.assignment.dataholders.ArticleRealm) objects.next();
            if(!cache.containsKey(object)) {
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo._idIndex, rowIndex, ((ArticleRealmRealmProxyInterface)object).realmGet$_id());
                String realmGet$title = ((ArticleRealmRealmProxyInterface)object).realmGet$title();
                if (realmGet$title != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title);
                }
                String realmGet$author = ((ArticleRealmRealmProxyInterface)object).realmGet$author();
                if (realmGet$author != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.authorIndex, rowIndex, realmGet$author);
                }
                String realmGet$scienceName = ((ArticleRealmRealmProxyInterface)object).realmGet$scienceName();
                if (realmGet$scienceName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.scienceNameIndex, rowIndex, realmGet$scienceName);
                }
                String realmGet$dop = ((ArticleRealmRealmProxyInterface)object).realmGet$dop();
                if (realmGet$dop != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.dopIndex, rowIndex, realmGet$dop);
                }
                String realmGet$image = ((ArticleRealmRealmProxyInterface)object).realmGet$image();
                if (realmGet$image != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image);
                }
                String realmGet$fbCount = ((ArticleRealmRealmProxyInterface)object).realmGet$fbCount();
                if (realmGet$fbCount != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.fbCountIndex, rowIndex, realmGet$fbCount);
                }
                String realmGet$twitterCount = ((ArticleRealmRealmProxyInterface)object).realmGet$twitterCount();
                if (realmGet$twitterCount != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.twitterCountIndex, rowIndex, realmGet$twitterCount);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.daffodil.assignment.dataholders.ArticleRealm object, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long tableNativePtr = table.getNativeTablePointer();
        ArticleRealmColumnInfo columnInfo = (ArticleRealmColumnInfo) realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long pkColumnIndex = table.getPrimaryKey();
        long rowIndex = TableOrView.NO_MATCH;
        Object primaryKeyValue = ((ArticleRealmRealmProxyInterface) object).realmGet$_id();
        if (primaryKeyValue != null) {
            rowIndex = table.findFirstLong(pkColumnIndex, ((ArticleRealmRealmProxyInterface) object).realmGet$_id());
        }
        if (rowIndex == TableOrView.NO_MATCH) {
            rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
            if (primaryKeyValue != null) {
                Table.nativeSetLong(tableNativePtr, pkColumnIndex, rowIndex, ((ArticleRealmRealmProxyInterface) object).realmGet$_id());
            }
        }
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo._idIndex, rowIndex, ((ArticleRealmRealmProxyInterface)object).realmGet$_id());
        String realmGet$title = ((ArticleRealmRealmProxyInterface)object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex);
        }
        String realmGet$author = ((ArticleRealmRealmProxyInterface)object).realmGet$author();
        if (realmGet$author != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.authorIndex, rowIndex, realmGet$author);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.authorIndex, rowIndex);
        }
        String realmGet$scienceName = ((ArticleRealmRealmProxyInterface)object).realmGet$scienceName();
        if (realmGet$scienceName != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.scienceNameIndex, rowIndex, realmGet$scienceName);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.scienceNameIndex, rowIndex);
        }
        String realmGet$dop = ((ArticleRealmRealmProxyInterface)object).realmGet$dop();
        if (realmGet$dop != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dopIndex, rowIndex, realmGet$dop);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.dopIndex, rowIndex);
        }
        String realmGet$image = ((ArticleRealmRealmProxyInterface)object).realmGet$image();
        if (realmGet$image != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.imageIndex, rowIndex);
        }
        String realmGet$fbCount = ((ArticleRealmRealmProxyInterface)object).realmGet$fbCount();
        if (realmGet$fbCount != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.fbCountIndex, rowIndex, realmGet$fbCount);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fbCountIndex, rowIndex);
        }
        String realmGet$twitterCount = ((ArticleRealmRealmProxyInterface)object).realmGet$twitterCount();
        if (realmGet$twitterCount != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.twitterCountIndex, rowIndex, realmGet$twitterCount);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.twitterCountIndex, rowIndex);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long tableNativePtr = table.getNativeTablePointer();
        ArticleRealmColumnInfo columnInfo = (ArticleRealmColumnInfo) realm.schema.getColumnInfo(com.daffodil.assignment.dataholders.ArticleRealm.class);
        long pkColumnIndex = table.getPrimaryKey();
        com.daffodil.assignment.dataholders.ArticleRealm object = null;
        while (objects.hasNext()) {
            object = (com.daffodil.assignment.dataholders.ArticleRealm) objects.next();
            if(!cache.containsKey(object)) {
                long rowIndex = TableOrView.NO_MATCH;
                Object primaryKeyValue = ((ArticleRealmRealmProxyInterface) object).realmGet$_id();
                if (primaryKeyValue != null) {
                    rowIndex = table.findFirstLong(pkColumnIndex, ((ArticleRealmRealmProxyInterface) object).realmGet$_id());
                }
                if (rowIndex == TableOrView.NO_MATCH) {
                    rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                    if (primaryKeyValue != null) {
                        Table.nativeSetLong(tableNativePtr, pkColumnIndex, rowIndex, ((ArticleRealmRealmProxyInterface) object).realmGet$_id());
                    }
                }
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo._idIndex, rowIndex, ((ArticleRealmRealmProxyInterface)object).realmGet$_id());
                String realmGet$title = ((ArticleRealmRealmProxyInterface)object).realmGet$title();
                if (realmGet$title != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex);
                }
                String realmGet$author = ((ArticleRealmRealmProxyInterface)object).realmGet$author();
                if (realmGet$author != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.authorIndex, rowIndex, realmGet$author);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.authorIndex, rowIndex);
                }
                String realmGet$scienceName = ((ArticleRealmRealmProxyInterface)object).realmGet$scienceName();
                if (realmGet$scienceName != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.scienceNameIndex, rowIndex, realmGet$scienceName);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.scienceNameIndex, rowIndex);
                }
                String realmGet$dop = ((ArticleRealmRealmProxyInterface)object).realmGet$dop();
                if (realmGet$dop != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.dopIndex, rowIndex, realmGet$dop);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.dopIndex, rowIndex);
                }
                String realmGet$image = ((ArticleRealmRealmProxyInterface)object).realmGet$image();
                if (realmGet$image != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.imageIndex, rowIndex, realmGet$image);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.imageIndex, rowIndex);
                }
                String realmGet$fbCount = ((ArticleRealmRealmProxyInterface)object).realmGet$fbCount();
                if (realmGet$fbCount != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.fbCountIndex, rowIndex, realmGet$fbCount);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.fbCountIndex, rowIndex);
                }
                String realmGet$twitterCount = ((ArticleRealmRealmProxyInterface)object).realmGet$twitterCount();
                if (realmGet$twitterCount != null) {
                    Table.nativeSetString(tableNativePtr, columnInfo.twitterCountIndex, rowIndex, realmGet$twitterCount);
                } else {
                    Table.nativeSetNull(tableNativePtr, columnInfo.twitterCountIndex, rowIndex);
                }
            }
        }
    }

    public static com.daffodil.assignment.dataholders.ArticleRealm createDetachedCopy(com.daffodil.assignment.dataholders.ArticleRealm realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.daffodil.assignment.dataholders.ArticleRealm unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.daffodil.assignment.dataholders.ArticleRealm)cachedObject.object;
            } else {
                unmanagedObject = (com.daffodil.assignment.dataholders.ArticleRealm)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.daffodil.assignment.dataholders.ArticleRealm();
            cache.put(realmObject, new RealmObjectProxy.CacheData(currentDepth, unmanagedObject));
        }
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$_id(((ArticleRealmRealmProxyInterface) realmObject).realmGet$_id());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$title(((ArticleRealmRealmProxyInterface) realmObject).realmGet$title());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$author(((ArticleRealmRealmProxyInterface) realmObject).realmGet$author());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$scienceName(((ArticleRealmRealmProxyInterface) realmObject).realmGet$scienceName());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$dop(((ArticleRealmRealmProxyInterface) realmObject).realmGet$dop());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$image(((ArticleRealmRealmProxyInterface) realmObject).realmGet$image());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$fbCount(((ArticleRealmRealmProxyInterface) realmObject).realmGet$fbCount());
        ((ArticleRealmRealmProxyInterface) unmanagedObject).realmSet$twitterCount(((ArticleRealmRealmProxyInterface) realmObject).realmGet$twitterCount());
        return unmanagedObject;
    }

    static com.daffodil.assignment.dataholders.ArticleRealm update(Realm realm, com.daffodil.assignment.dataholders.ArticleRealm realmObject, com.daffodil.assignment.dataholders.ArticleRealm newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$title(((ArticleRealmRealmProxyInterface) newObject).realmGet$title());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$author(((ArticleRealmRealmProxyInterface) newObject).realmGet$author());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$scienceName(((ArticleRealmRealmProxyInterface) newObject).realmGet$scienceName());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$dop(((ArticleRealmRealmProxyInterface) newObject).realmGet$dop());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$image(((ArticleRealmRealmProxyInterface) newObject).realmGet$image());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$fbCount(((ArticleRealmRealmProxyInterface) newObject).realmGet$fbCount());
        ((ArticleRealmRealmProxyInterface) realmObject).realmSet$twitterCount(((ArticleRealmRealmProxyInterface) newObject).realmGet$twitterCount());
        return realmObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("ArticleRealm = [");
        stringBuilder.append("{_id:");
        stringBuilder.append(realmGet$_id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{title:");
        stringBuilder.append(realmGet$title());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{author:");
        stringBuilder.append(realmGet$author());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{scienceName:");
        stringBuilder.append(realmGet$scienceName());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{dop:");
        stringBuilder.append(realmGet$dop());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{image:");
        stringBuilder.append(realmGet$image());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fbCount:");
        stringBuilder.append(realmGet$fbCount());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{twitterCount:");
        stringBuilder.append(realmGet$twitterCount());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleRealmRealmProxy aArticleRealm = (ArticleRealmRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aArticleRealm.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aArticleRealm.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aArticleRealm.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
