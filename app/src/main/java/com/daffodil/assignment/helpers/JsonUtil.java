package com.daffodil.assignment.helpers;

import com.daffodil.assignment.dataholders.ApplicationConstants;
import com.daffodil.assignment.dataholders.Article;
import com.daffodil.assignment.dataholders.ArticleRealm;
import com.daffodil.assignment.dataholders.VHDetailHeader;
import com.daffodil.assignment.dataholders.VHSimilarItems;
import com.daffodil.assignment.dataholders.VHWebView;
import com.daffodil.assignment.listeners.OnWebViewClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonUtil {

//    public static Article jsonObjectToArticle(JSONObject passedJsonObject, int i) {
//        Article newArticle = new Article();
//        newArticle.title = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TITLE);
//        newArticle.author = passedJsonObject.optString(ApplicationConstants.JSON_TAG_AUTHOR);
//        newArticle.scienceName = passedJsonObject.optString(ApplicationConstants.JSON_TAG_NAME);
//        newArticle.dop = passedJsonObject.optString(ApplicationConstants.JSON_TAG_DOP);
//        newArticle.dop = HelperMethods.dateConverter(newArticle.dop, i);
//        newArticle.image = passedJsonObject.optString(ApplicationConstants.JSON_TAG_IMAGE);
//        newArticle.fbCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_FB_COUNT);
//        newArticle.twitterCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TWITTER_COUNT);
//        return newArticle;
//    }

    public static JSONArray stringToJsonArray(String jsonString) {
        JSONObject myJsonObject = null;
        try {
            myJsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject myJsonObject1;
        if (myJsonObject != null) {
            myJsonObject1 = myJsonObject.optJSONObject("result");
            return myJsonObject1.optJSONArray("articles");
        } else {
            return null;
        }
    }

    public static JSONObject stringToJsonObject(String jsonString) {
        JSONObject myJsonObject = null;
        try {
            myJsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject myJsonObject1;
        if (myJsonObject != null) {
            myJsonObject1 = myJsonObject.optJSONObject("result");
            return myJsonObject1;
        } else {
            return null;
        }
    }

//    public static ArrayList<Article> jsonDataToArrayList(String jsonDataString) {
//        JSONArray jsonArray = JsonUtil.stringToJsonArray(jsonDataString);
//        if (jsonArray != null) {
//            ArrayList<Article> articleList = new ArrayList<>(jsonArray.length());
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject jsonObject;
//                try {
//                    jsonObject = jsonArray.getJSONObject(i);
//                    Article receivedJsonArticle;
//                    receivedJsonArticle = JsonUtil.jsonObjectToArticle(jsonObject, i);
//                    articleList.add(receivedJsonArticle);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            return articleList;
//        } else {
//            return null;
//        }
//
//    }

    public static ArrayList<ArticleRealm> jsonDataToArrayList2(String jsonDataString) {

        JSONArray jsonArray = JsonUtil.stringToJsonArray(jsonDataString);
        if (jsonArray != null) {
            ArrayList<ArticleRealm> articleList = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArray.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ArticleRealm receivedJsonArticle;
                receivedJsonArticle = JsonUtil.jsonObjectToArticle2(jsonObject, i);
                articleList.add(receivedJsonArticle);
            }
            return articleList;
        } else {
            return null;
        }
    }

    public static ArticleRealm jsonObjectToArticle2(JSONObject passedJsonObject, int i) {
        ArticleRealm newArticle = new ArticleRealm();
        newArticle.setTitle(passedJsonObject.optString(ApplicationConstants.JSON_TAG_TITLE));
        newArticle.setAuthor(passedJsonObject.optString(ApplicationConstants.JSON_TAG_AUTHOR));
        newArticle.setScienceName(passedJsonObject.optString(ApplicationConstants.JSON_TAG_NAME));
        newArticle.setDop(passedJsonObject.optString(ApplicationConstants.JSON_TAG_DOP));
        newArticle.setDop(HelperMethods.dateConverter(newArticle.getDop(), i));
        newArticle.setImage(passedJsonObject.optString(ApplicationConstants.JSON_TAG_IMAGE));
        newArticle.setFbCount(passedJsonObject.optString(ApplicationConstants.JSON_TAG_FB_COUNT));
        newArticle.setTwitterCount(passedJsonObject.optString(ApplicationConstants.JSON_TAG_TWITTER_COUNT));

        newArticle.set_id(passedJsonObject.optInt(ApplicationConstants.JSON_TAG_ID));
        return newArticle;
    }

    public static VHDetailHeader getMainArticle(String json) {
        JSONObject jsonObject = stringToJsonObject(json);
        VHDetailHeader detailHeader = jsonObjectToArticlex1(jsonObject, 0);
        return detailHeader;
    }

    private static Article jsonObjectToArticle(JSONObject passedJsonObject, int i) {
        Article newArticle = new Article();
        newArticle.title = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TITLE);
        newArticle.author = passedJsonObject.optString(ApplicationConstants.JSON_TAG_AUTHOR);
        newArticle.scienceName = passedJsonObject.optString(ApplicationConstants.JSON_TAG_NAME);
        newArticle.dop = passedJsonObject.optString(ApplicationConstants.JSON_TAG_DOP);
        newArticle.dop = HelperMethods.dateConverter(newArticle.dop, i);
        newArticle.image = passedJsonObject.optString(ApplicationConstants.JSON_TAG_IMAGE);
        newArticle.fbCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_FB_COUNT);
        newArticle.twitterCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TWITTER_COUNT);

        newArticle._id = passedJsonObject.optInt(ApplicationConstants.JSON_TAG_ID);
        return newArticle;
    }
    private static VHDetailHeader jsonObjectToArticlex1(JSONObject passedJsonObject, int i) {
        VHDetailHeader newArticle = new VHDetailHeader();
        newArticle.title = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TITLE);
        newArticle.author = passedJsonObject.optString(ApplicationConstants.JSON_TAG_AUTHOR);
        newArticle.scienceName = passedJsonObject.optString(ApplicationConstants.JSON_TAG_NAME);
        newArticle.dop = passedJsonObject.optString(ApplicationConstants.JSON_TAG_DOP);
        newArticle.dop = HelperMethods.dateConverter(newArticle.dop, i);
        newArticle.image = passedJsonObject.optString(ApplicationConstants.JSON_TAG_IMAGE);
        newArticle.fbCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_FB_COUNT);
        newArticle.twitterCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TWITTER_COUNT);

        newArticle._id = passedJsonObject.optInt(ApplicationConstants.JSON_TAG_ID);
        return newArticle;
    }
    private static VHSimilarItems jsonObjectToArticlex2(JSONObject passedJsonObject, int i) {
        VHSimilarItems newArticle = new VHSimilarItems();
        newArticle.title = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TITLE);
        newArticle.author = passedJsonObject.optString(ApplicationConstants.JSON_TAG_AUTHOR);
        newArticle.scienceName = passedJsonObject.optString(ApplicationConstants.JSON_TAG_NAME);
        newArticle.dop = passedJsonObject.optString(ApplicationConstants.JSON_TAG_DOP);
        newArticle.dop = HelperMethods.dateConverter(newArticle.dop, i);
        newArticle.image = passedJsonObject.optString(ApplicationConstants.JSON_TAG_IMAGE);
        newArticle.fbCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_FB_COUNT);
        newArticle.twitterCount = passedJsonObject.optString(ApplicationConstants.JSON_TAG_TWITTER_COUNT);

        newArticle._id = passedJsonObject.optInt(ApplicationConstants.JSON_TAG_ID);
        return newArticle;
    }

    public static VHWebView getDetail(String json, OnWebViewClickListener listener) {
        JSONObject jsonObject = stringToJsonObject(json);
        VHWebView webView = new VHWebView(listener);
        try {
            if (jsonObject != null) {
                webView.vewHtml = jsonObject.getString(ApplicationConstants.JSON_TAG_HTML);
            }else{
                webView.vewHtml = "<p>Sorry unable to view html1</p>";
            }
        } catch (JSONException e) {
            webView.vewHtml = "<p>Sorry unable to view html2</p>";
            e.printStackTrace();
        }
        return webView;
    }

    public static ArrayList<VHSimilarItems> getSimilarItemsList(String json) {
        JSONArray jsonArray = stringToJsonArray(json);
        if (jsonArray != null) {
            ArrayList<VHSimilarItems> articleList = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = jsonArray.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                VHSimilarItems receivedJsonArticle;
                receivedJsonArticle = JsonUtil.jsonObjectToArticlex2(jsonObject, 1);
                articleList.add(receivedJsonArticle);
            }
            return articleList;
        } else {
            return null;
        }
    }
}
